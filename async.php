<?php

require_once __DIR__ . '/vendor/autoload.php';

use Aws\Ec2\Ec2Client;
use GuzzleHttp\Promise\Promise;

$ec2Client = new Ec2Client([
    'region' => 'us-east-1',
    'version' => 'latest',
    'profile' => 'ec2manager'
]);

$result = $ec2Client->describeRegionsAsync();

$result->then(
    function ($value) {
        //echo "The promise was fulfilled with {$value}";
        return $value->get('Regions');
    },
    function ($reason) {
        echo "The promise was rejected with {$reason}";
    }
)->then(
    function ($value){
        print_r($value);
    }
);

echo 123;


// Wait for the operation to complete
$result->wait();

