<?php
require_once __DIR__ . '/vendor/autoload.php';
$client = new EC2\Manager\Ec2Manager();
$regions = $client->getAllRegions();
?>
<table border="1" width="100%">
    <tr>
        <td>Index</td>
        <td>Region</td>
        <td>Ec2 Instances</td>
        <td>RDS Instances</td>
    </tr>
    <?php
    if (!empty($regions)) {
        foreach ($regions as $key => $region) {

            ?>
            <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo $region['RegionName']; ?></td>
                <td><a href="view-instances.php?region=<?php echo $region['RegionName']; ?>">View</a></td>
                <td><a href="rds-instances.php?region=<?php echo $region['RegionName']; ?>">View</a></td>
            </tr>
            <?php
        }
    }
    ?>
</table>
