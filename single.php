<?php
require_once __DIR__ . '/vendor/autoload.php';
$client = new EC2\Manager\Ec2Manager();
$results = $client->getInstances();
?>
<table border="1" width="700">
    <tr>
        <td></td>
        <td>InstanceId</td>
        <td>Instance Type</td>
        <td>Region</td>
        <td>Private IP</td>
        <td>Public IP</td>
        <td>State</td>
        <td>Action</td>
    </tr>
    <?php
    include 'loop.php';
    ?>
</table>
