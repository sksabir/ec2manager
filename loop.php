<?php
if (!empty($results)) {
    foreach ($results as $key => $result) {

        $name = $result['Instances'][0]['Tags'][0]['Value'];
        $instanceId = $result['Instances'][0]['InstanceId'];
        $instanceType =$result['Instances'][0]['InstanceType'];
        $zone =$result['Instances'][0]['Placement']['AvailabilityZone'];
        $instanceState = $result['Instances'][0]['State']['Name'];
        //$statusCheck = $result['Instances'][0]['State']['Name'];
        $publicDns = $result['Instances'][0]['PublicDnsName'];
        $publicIpAddress = $result['Instances'][0]['PublicIpAddress'];
        $keyName = $result['Instances'][0]['KeyName'];
        $monitoring = $result['Instances'][0]['Monitoring']['State'];
        //$lunchTime = $result['Instances'][0]['LaunchTime']->format(\DateTime::ISO8601);
        $securityGroups = $result['Instances'][0]['SecurityGroups'][0]['GroupName'];
        $privateIpAddress = $result['Instances'][0]['PrivateIpAddress'];

        $action = 'enable';
        $instanceStateClass = 'disable';
        $monitoringClass = 'enable';
        if($instanceState == 'running'){
            $action = 'disable';
            $instanceStateClass = 'enable';
        }
        if($monitoring == 'disabled'){
            $monitoringClass = 'disable';
        }
        ?>

        <tr>
            <td><?php echo $key+1; ?></td>
            <td><?php echo $name; ?></td>
            <td><?php echo $instanceId; ?></td>
            <td><?php echo $instanceType; ?></td>
            <td><?php echo $zone; ?></td>
            <td class="<?php echo $instanceStateClass; ?>"><?php echo $instanceState; ?></td>
            <td><?php echo $publicDns; ?></td>
            <td><?php echo $publicIpAddress; ?></td>
            <td><?php echo $keyName; ?></td>
            <td class="<?php echo $monitoringClass; ?>"><?php echo $monitoring; ?></td>
<!--            <td>--><?php //echo $lunchTime; ?><!--</td>-->
            <td><?php echo $securityGroups; ?></td>
            <td><?php echo $privateIpAddress; ?></td>
            <td><a href="action.php?instanceId=<?php echo $instanceId; ?>&action=<?php echo $action; ?>"><?php echo ucfirst($action); ?></a></td>
        </tr>
        <?php

    }
}
?>