<?php
require_once __DIR__ . '/vendor/autoload.php';
$client = new EC2\Manager\RdsManager($_GET['region']);
$results = $client->getDbInstances();
echo "<pre>";print_r($results);die();
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        .enable{
            color: green;
        }
        .disable{
            color:red;
        }
    </style>
</head>
<body>


<table id="customers">
    <tr>
        <td>Index</td>
        <td>DB Instance</td>
        <td>Engine</td>
    </tr>
    <?php
    if (!empty($results)) {
        foreach ($results as $key => $result) {

            $dBInstanceIdentifier = $result['DBInstanceIdentifier'];
            $engine = $result['Engine'];
            ?>

            <tr>
                <td><?php echo $key+1; ?></td>
                <td><?php echo $dBInstanceIdentifier; ?></td>
                <td><?php echo $engine; ?></td>
            </tr>
            <?php

        }
    }
    ?>
</table>

</body>
</html>
