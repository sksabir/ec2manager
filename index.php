<?php
require_once __DIR__ . '/vendor/autoload.php';
$client = new EC2\Manager\Ec2Manager();
$regions = $client->getAllRegions();
?>
<table border="1" width="700">
    <tr>
        <td></td>
        <td>InstanceId</td>
        <td>Instance Type</td>
        <td>Region</td>
        <td>Private IP</td>
        <td>Public IP</td>
        <td>State</td>
        <td>Action</td>
    </tr>
    <?php
    if (!empty($regions)) {
        foreach ($regions as $region) {
            //echo $region['RegionName'];die();
            $client = new EC2\Manager\Ec2Manager($region['RegionName']);
            $results = $client->getInstances();

            include 'loop.php';

        }
    }
    ?>
</table>
