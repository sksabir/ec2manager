<?php

namespace EC2\Manager;

class RdsManager
{
    public $client;

    public $region;

    public function __construct($region='')
    {
        if(empty($region)){
            $region = 'ap-southeast-1';
        }

        $this->client = new \Aws\Rds\RdsClient([
            'region' => $region,
            'profile' => 'ec2manager',
            'version' => 'latest',
        ]);

        $this->region=$region;
    }

    public function getAllRegions(){
        return $this->client->describeRegions()->get('Regions');
    }

    public function getDbInstances()
    {
        return $this->client->describeDBInstances()->get('DBInstances');
    }

    public function enableInstance($instanceIds = array())
    {
//        return $this->ec2Client->startInstances(array(
//            'InstanceIds' => $instanceIds,
//        ));
    }

    public function disableInstance($instanceIds = array())
    {
//        return $this->ec2Client->stopInstances(array(
//            'InstanceIds' => $instanceIds,
//        ));
    }
}