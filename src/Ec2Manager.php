<?php

namespace EC2\Manager;

class Ec2Manager
{
    public $ec2Client;

    public $region;

    public function __construct($region='')
    {
        if(empty($region)){
            $region = 'us-east-1';
        }

        $this->ec2Client = new \Aws\Ec2\Ec2Client([
            'region' => $region,
            'profile' => 'ec2manager',
            'version' => 'latest',
        ]);

        $this->region=$region;
    }

    public function getAllRegions(){
        return $this->ec2Client->describeRegions()->get('Regions');
    }

    public function getInstances()
    {
        return $this->ec2Client->describeInstances()->get('Reservations');
    }

    public function enableInstance($instanceIds = array())
    {
//        return $this->ec2Client->startInstances(array(
//            'InstanceIds' => $instanceIds,
//        ));
    }

    public function disableInstance($instanceIds = array())
    {
//        return $this->ec2Client->stopInstances(array(
//            'InstanceIds' => $instanceIds,
//        ));
    }
}