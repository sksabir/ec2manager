<?php
require_once __DIR__ . '/vendor/autoload.php';

$obj = new EC2\Manager\Ec2Manager();
if(!empty($_GET['action']) && $_GET['action']=='enable'){
    $instance = array(
        'InstanceIds' => $_GET['instanceId']
    );
    $obj->enableInstance($instance);
}

if(!empty($_GET['action']) && $_GET['action']=='disable'){

    $instance = array(
        'InstanceIds' => $_GET['instanceId']
    );
    $obj->disableInstance($instance);

}
header('Location: '.$_SERVER['HTTP_REFERER']);
exit;

