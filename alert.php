<?php
require_once __DIR__ . '/vendor/autoload.php';

$args = [
    'region' => 'us-east-1',
    'profile' => 'ec2manager',
    'version' => 'latest',
];
$cloudWatchClient = new Aws\CloudWatch\CloudWatchClient($args);

$ec2Client = new Aws\Ec2\Ec2Client($args);
$results = $ec2Client->describeInstances()->get('Reservations');

$promises = [];
$instanceIds = [];
if (!empty($results)) {
    foreach ($results as $key => $result) {
        $instanceIds[] = $result['Instances'][0]['InstanceId'];
        $promises[] = $cloudWatchClient->getMetricStatisticsAsync([
            'Dimensions' => [
                [
                    'Name' => 'InstanceId',
                    'Value' => $result['Instances'][0]['InstanceId']
                ]
            ],
            'MetricName' => 'CPUUtilization', // REQUIRED
            'Namespace' => 'AWS/EC2', // REQUIRED
            'Period' => 900, // REQUIRED
            'StartTime' => strtotime('-1 hours'),
            'EndTime' => strtotime('now'),
            'Statistics' => ['Maximum']
        ]);
    }
}


\GuzzleHttp\Promise\all($promises)->then(function ($responses) use ($instanceIds) {
    foreach ($responses as $key => $response) {
        $dataPoints = $response['Datapoints'];
        $cpuUtilization = maxValueInArray($dataPoints,'Maximum');
        if($cpuUtilization > 70){
            sendMail($cpuUtilization,$instanceIds[$key]);
        }
    }
})->wait();

function maxValueInArray($array, $keyToSearch)
{
    $currentMax = NULL;
    foreach($array as $arr)
    {
        foreach($arr as $key => $value)
        {
            if ($key == $keyToSearch && ($value >= $currentMax))
            {
                $currentMax = $value;
            }
        }
    }

    return $currentMax;
}

function sendMail($cpuUtilization,$instanceId){
    //mail function goes here
    $to      = 'nobody@example.com';
    $subject = 'the subject';
    $message = 'InstanceID: '.$instanceId.' CPU % '.$cpuUtilization.'';
    $headers = 'From: webmaster@example.com' . "\r\n" .
        'Reply-To: webmaster@example.com' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);
}






