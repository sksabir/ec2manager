<!DOCTYPE html>
<html>
<head>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        .enable{
            color: green;
        }
        .disable{
            color:red;
        }
    </style>
</head>
<body>

<?php
require_once __DIR__ . '/vendor/autoload.php';
$client = new EC2\Manager\Ec2Manager($_GET['region']);
$results = $client->getInstances();

?>
<table id="customers">
    <tr>
        <td>Index</td>
        <td>Name</td>
        <td>InstanceId</td>
        <td>Instance Type</td>
        <td>Availability Zone</td>
        <td>Instance State</td>
        <td>Public DNS(IPv4)</td>
        <td>IPv4 Public IP</td>
        <td>Key Name</td>
        <td>Monitoring</td>
        <td>Security Groups</td>
        <td>Private IP</td>
        <td>Action</td>
    </tr>
    <?php
    include 'loop.php';
    ?>
</table>

</body>
</html>
